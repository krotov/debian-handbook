msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2020-05-17 17:54+0200\n"
"PO-Revision-Date: 2015-10-01 18:00+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Jaroslav Tesař\n"
"Language: cs-CZ \n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Publican v4.3.2\n"

msgid "Falcot Corp"
msgstr "Falcot Corp"

msgid "SMB"
msgstr "SMB"

msgid "Strong Growth"
msgstr "Silný růst"

msgid "Master Plan"
msgstr "Hlavní plán"

msgid "Migration"
msgstr "Přesun"

msgid "Cost Reduction"
msgstr "Úsora nákladů"

msgid "Presenting the Case Study"
msgstr "Představení případové studie"

#, fuzzy
#| msgid "In the context of this book, you are the system administrator of a growing small business. The time has come for you to redefine the information systems master plan for the coming year in collaboration with your directors. You choose to progressively migrate to Debian, both for practical and economical reasons. Let's see in more detail what's in store for you…"
msgid "In the context of this book, you are the system administrator of a growing small business. The time has come for you to redefine the information systems master plan for the coming year in collaboration with your directors. You choose to progressively migrate to Debian, both for practical and economical reasons. Let's see in more detail what is in store for you…"
msgstr "V kontextu této knihy jste systémový administrátor malého rostoucího podniku. Nadešel pro vás čas pro znovustanovení hlavního plánu informačního systému pro nadcházející rok ve spolupráci s vašimi řediteli. Zvolili jste postupně přejít na Debian, zároveň z praktických i ekonomických důvoků. Podívejme se podrobněji, co tu pro vás máme..."

msgid "We have envisioned this case study to approach all modern information system services currently used in a medium sized company. After reading this book, you will have all of the elements necessary to install Debian on your servers and fly on your own wings. You will also learn how to efficiently find information in the event of difficulties."
msgstr "Sestavili jsme tuto případovou studii za účelem přiblížení všech moderních služeb v současnosti používaných v informačních systémech středně velké firmy. Po přečtení této knihy budete mít všechny prvky nutné k instalaci Debianu na vaše servery a být svým vlastním pánem. Také se naučíte, jak efektivně najít informace v případě těžkostí."

msgid "Fast Growing IT Needs"
msgstr "Rychle rostoucí požadavky na IT"

msgid "Falcot Corp is a manufacturer of high quality audio equipment. The company is growing strongly, and has two facilities, one in Saint-Étienne, and another in Montpellier. The former has around 150 employees; it hosts a factory for the manufacturing of speakers, a design lab, and all administrative office. The Montpellier site is smaller, with only about 50 workers, and produces amplifiers."
msgstr "Falcot Corp je výrobce vysoce kvalitního audio vybavení. Firma rychle roste a má dvě provozovny, jednu v Saint-Étienne a druhou v Montpellier. Ta první jmenovaná má asi 150 zaměstnanců a poskytuje zázemítovárně na výrobu reproduktorů, designové lanoratoři a všem kancelářím pro administrativu. Pobočka v Montpellier je menší, s asi padesáti zaměstanci a vyrábí zesilovače."

msgid "<emphasis>NOTE</emphasis> Fictional company created for case study"
msgstr "<emphasis>POZNÁMKA</emphasis> Smyšlená firma vytvořená pro tuto případovou studii"

msgid "The Falcot Corp company used as an example here is completely fictional. Any resemblance to an existing company is purely coincidental. Likewise, some example data throughout this book may be fictional."
msgstr "Firma Falcot Corp, která je zde uvedená jako příklad je zcela smyšlená. Jakákoliv podobnost s existující firmou je čistě náhodná. Podobně, data příkladů v této knize mohou být fiktivní."

#, fuzzy
#| msgid "The computer system has had difficulty keeping up with the company's growth, so they are now determined to completely redefine it to meet various goals established by management:"
msgid "The information system has had difficulty keeping up with the company's growth, so they are now determined to completely redefine it to meet various goals established by management:"
msgstr "Počítačový systém měl potíže držet krok s růstem podniku, takže je rozhodnuto jej úplně předefinovat k dosažení různých cílů stanovených managementem:"

msgid "modern, easily scalable infrastructure;"
msgstr "modenrní, snadno rozšiřitelná infrastruktura;"

msgid "reducing cost of software licenses thanks to use of Open Source software;"
msgstr "snížit náklady na softwarové licence, díky užívání Open Source softwaru;"

msgid "installation of an e-commerce website, possibly B2B (business to business, i.e. linking of information systems between different companies, such as a supplier and its clients);"
msgstr "zřízení internetové stránky pro elektronickou komerci, pravděpodobně B2B (bussiness to bussiness, tedy propojení informačních systémů mezi různými podniky, jako dodavatele a jeho klienta;"

msgid "significant improvement in security to better protect trade secrets related to new products."
msgstr "podstatné zdokonalení v bezbečnosti k lepšímu zabezpečení obchodních tajemství souvisejících s novými výrobky."

msgid "The entire information system will be overhauled with these goals in mind."
msgstr "Celý informační systém bude důkladně prohlédnut a opraven v duchu těchto cílů."

msgid "<primary>master plan</primary>"
msgstr "<primary>hlavní plán</primary>"

msgid "<primary>migration</primary>"
msgstr "<primary>přechod</primary>"

msgid "With your collaboration, IT management has conducted a slightly more extensive study, identifying some constraints and defining a plan for migration to the chosen Open Source system, Debian."
msgstr "Za vaší spoluúčasti, IT management sestavil mírně rozsáhlou studii, pojmenovávající omezení a stanovující plán přechodu na Open Source systém, Debian."

msgid "A significant constraint identified is that the accounting department uses specific software, which only runs on <trademark>Microsoft Windows</trademark>. The laboratory, for its part, uses computer aided design software that runs on <trademark>OS X</trademark>."
msgstr "Podstatným jmenovaným omezením je, že účtárna používá zvláštní software, který běží pouze na <trademark>Microsoft Windows</trademark>. Laboratoř, pro změnu, používá počítačový designový software, který funguje na <trademark>OS X</trademark>."

msgid "Overview of the Falcot Corp network"
msgstr "Všeobecný náhled na síť Falcot Corp"

msgid "The switch to Debian will be gradual; a small business, with limited means, cannot reasonably change everything overnight. For starters, the IT staff must be trained in Debian administration. The servers will then be converted, starting with the network infrastructure (routers, firewalls, etc.) followed by the user services (file sharing, Web, SMTP, etc.). Then the office computers will be gradually migrated to Debian, for each department to be trained (internally) during the deployment of the new system."
msgstr "Přechod na Debian bude postupný; malý podnik, s omezenými prostředky, nemůže z dobrých důvodů změnit všechno přes noc. Pro začátečníky, IT zaměstnanci musí být proškoleni ve správcování Debianu. Servery budou přestavěny, počínaje síťovou infrastrukturou (routery, firewally apod.), následovány uživatelskými službami (sdílením souborů, prohlížením internetových stránek, SMTP apod.). Potom budou kancelářské počítače postupně přestavěny na Debian a každé oddělení bude během provádění přechodu interně proškolováno na nový systém."

msgid "Why a GNU/Linux Distribution?"
msgstr "Proč GNU/Linuxová distribuce?"

msgid "<emphasis>BACK TO BASICS</emphasis> Linux or GNU/Linux?"
msgstr "<emphasis>ZPĚT K ZÁKLADŮM</emphasis> Linux nebo GNU/Linux?"

msgid "<primary>GNU/Linux</primary>"
msgstr "<primary>GNU/Linux</primary>"

msgid "<primary>Linux</primary>"
msgstr "<primary>Linux</primary>"

msgid "Linux, as you already know, is only a kernel. The expressions, “Linux distribution” and “Linux system” are, thus, incorrect: they are, in reality, distributions or systems <emphasis>based on</emphasis> Linux. These expressions fail to mention the software that always completes this kernel, among which are the programs developed by the GNU Project. Dr. Richard Stallman, founder of this project, insists that the expression “GNU/Linux” be systematically used, in order to better recognize the important contributions made by the GNU Project and the principles of freedom upon which they are founded."
msgstr "Linux, jak už asi víte, je jádro. Výrazy “Linuxová distribuce” a “Linuxový systém” jsou proto nesprávné: ve skutečnosti jsou distribuce nebo systémy <emphasis>založené na</emphasis> Linuxu. Tyto výrazy postrádají zmínku o softwaru, který vždy doplňuje jádro, kolem kterého jsou programy vyvíjeny GNU Projectem. Dr. Richard Stallman, zakladatel tohoto projektu, trvá na tom, aby byl výraz “GNU/Linux” pravidelně používán, pro lepší rozlišení důležitosti příspěvků vytvořených GNU Projectem a principů svobody, na kterých jsou postaveny."

#, fuzzy
#| msgid "Debian has chosen to follow this recommendation, and, thus, name its distributions accordingly (thus, the latest stable release is Debian GNU/Linux 8)."
msgid "Debian has chosen to follow this recommendation, and, thus, name its distributions accordingly (thus, the latest stable release is Debian GNU/Linux 10)."
msgstr "Debian se rozhodl následovat toto doporučení, a tak se jeho distribuce podle toho nazývají (takže, poslední stabilní distribuce je Debian GNU/Linux 8)."

msgid "Several factors have dictated this choice. The system administrator, who was familiar with this distribution, ensured it was listed among the candidates for the computer system overhaul. Difficult economic conditions and ferocious competition have limited the budget for this operation, despite its critical importance for the future of the company. This is why Open Source solutions were swiftly chosen: several recent studies indicate they are less expensive than proprietary solutions while providing equal or better quality of service so long as qualified personnel are available to run them."
msgstr "Další faktory ovlivnily tuto volbu. Správce systému, který byl obeznámen s touto distribucí, se ujistil, že byla zapsána mezi kandidáty na generáku počítačového systému. Těžké ekonomické podmínky a lítý konkurenční boj limitovaly rozpočet na tuto operaci, navzdory její kritické důležitosti pro budoucnost podniku. Toto je důvod, proč byla Open Source řešení okamžitě vybrána: několik současných studií naznačuje, že jsou levnější než proprietální řešení, zatímco poskytují rovnocenou nebo lepší kvalitu služeb, pokud je kvalifikovaný personál schopen je provozovat."

msgid "<emphasis>IN PRACTICE</emphasis> Total cost of ownership (TCO)"
msgstr "<emphasis>V PRAXI</emphasis> Celkové náklady na vlastnictví (TCO)"

msgid "<primary>TCO</primary>"
msgstr "<primary>TCO</primary>"

msgid "<primary>Total Cost of Ownership</primary>"
msgstr "<primary>Celkové náklady na vlastnictví</primary>"

msgid "The Total Cost of Ownership is the total of all money expended for the possession or acquisition of an item, in this case referring to the operating system. This price includes any possible license fee, costs for training personnel to work with the new software, replacement of machines that are too slow, additional repairs, etc. Everything arising directly from the initial choice is taken into account."
msgstr "Celkové náklady na vlastnictní jsou veškeré peníze vynaložené na držení a pořízení položky, v tomto případě mluvíme o operačním systému. Tato cena zahrnuje všechny možné licenční poplatky, náklady na školenení personálu kvůli práci s novým softwarem, nahrazení příliš pomalých strojů, další opravy apod. Všechno, co přímo vzešlo z počáteční rozhodnutí je bráno v potaz."

msgid "This TCO, which varies according to the criteria chosen in the assessment thereof, is rarely significant when taken in isolation. However, it is very interesting to compare TCOs for different options if they are calculated according to the same rules. This assessment table is, thus, of paramount importance, and it is easy to manipulate it in order to draw a predefined conclusion. Thus, the TCO for a single machine doesn't make sense, since the cost of an administrator is also reflected in the total number of machines they manage, a number which obviously depends on the operating system and tools proposed."
msgstr "Tyto TCO, které se liší podle kritérií zvolených při jejich posouzení, jsou zřídka vypovídající, pokud jsou posuzovány odděleně. Každopádně, je velmi zajímavé srovnat TCO pro různé možnosti, pokud jsou vypočítány podle stejných pravidel. Taková hodnotící tabulka nabývá potom zásadní důležitosti a je velmi snadné s ní manipulovat za tím účelem, aby vedla k předem definovaným výsledkům. Takže, TCO pro jedno zařízní nemá smysl, protože náklady na správce odráží také celkový počet zařízení, které obsluhuje, počet, který zjevně závisí na operačním systému a navržených nástrojích."

msgid "Among free operating systems, the IT department looked at the free BSD systems (OpenBSD, FreeBSD, and NetBSD), GNU Hurd, and Linux distributions. GNU Hurd, which has not yet released a stable version, was immediately rejected. The choice is simpler between BSD and Linux. The former have many merits, especially on servers. Pragmatism, however, led to choosing a Linux system, since its installed base and popularity are both very significant and have many positive consequences. One of these consequences is that it is easier to find qualified personnel to administer Linux machines than technicians experienced with BSD. Furthermore, Linux adapts to newer hardware faster than BSD (although they are often neck and neck in this race). Finally, Linux distributions are often more adapted to user-friendly graphical user interfaces, indispensable for beginners during migration of all office machines to a new system."
msgstr "Co do volných operačních systémlů, IT oddělení posuzovalo BSD systémy (OpenBSD, FreeBSD, a NetBSD), GNU Hurd, a Linuxové distribuce. GNU Hurd, který ještě nevydal stabilní verzi, byl okamžitě zamítnut. Volba mezi BSD a Linuxem je snadná. První má hodně předností, především na serverech. Pragmatismus, nicméně, vedl k výběru Linuxového systému, protože obojí - jeho instalační základna a popularita mají velký význam a mají hodně pozitivních důsledků. Jedem z těchto důsledků je ten, že je snazší najít kvalifikovaný personál ke správcování zařízení, než techniky se zkušenostmi s BSD. Navíc, Linux se přizpůsobuje novému hardwaru rychleji, než BSD (i když je to často v tomto závodu hodně těsné). Na závěr, Linuxové distribuce jsou často lépe přizpůsobeny pro uživatelsky přívětivá grafická rozhraní, nezbytná pro začátečníky při přechodu všech kancelářských zařízaní na nový systém."

msgid "<emphasis>ALTERNATIVE</emphasis> Debian GNU/kFreeBSD"
msgstr "<emphasis>ALTERNATIVA</emphasis> Debian GNU/kFreeBSD"

msgid "<primary>kFreeBSD</primary>"
msgstr "<primary>kFreeBSD</primary>"

msgid "<primary>FreeBSD</primary>"
msgstr "<primary>FreeBSD</primary>"

msgid "<primary>BSD</primary>"
msgstr "<primary>BSD</primary>"

#, fuzzy
#| msgid "<primary>migration</primary>"
msgid "<primary><literal>ports.debian.org</literal></primary>"
msgstr "<primary>přechod</primary>"

#, fuzzy
#| msgid "Since Debian <emphasis role=\"distribution\">Squeeze</emphasis>, it is possible to use Debian with a FreeBSD kernel on 32 and 64 bit computers; this is what the <literal>kfreebsd-i386</literal> and <literal>kfreebsd-amd64</literal> architectures mean. While these architectures are not “official release architectures”, about 90 % of the software packaged by Debian is available for them."
msgid "Since Debian 6 <emphasis role=\"distribution\">Squeeze</emphasis>, it is possible to use Debian with a FreeBSD kernel on 32 and 64 bit computers; this is what the <literal>kfreebsd-i386</literal> and <literal>kfreebsd-amd64</literal> architectures mean. While these architectures are not “official” (they are hosted on a separate mirror — <literal>ports.debian.org</literal>), they provide over 70% of the software packaged by Debian."
msgstr "Od Debianu <emphasis role=\"distribution\">Squeeze</emphasis>, je možné používat Debian s jádrem FreeBSD na 32 nebo 64 bitových počítačích; to je to, co <literal>kfreebsd-i386</literal> a <literal>kfreebsd-amd64</literal> znamenají. I když tyto architektury nejsou “oficiálními architekturami pro vydání”, více, jak 90 % softwaru nabalíčkovaného Debianem je pro ně k dispozici."

msgid "These architectures may be an appropriate choice for Falcot Corp administrators, especially for a firewall (the kernel supports three different firewalls: IPF, IPFW, PF) or for a NAS (network attached storage system, for which the ZFS filesystem has been tested and approved)."
msgstr "Tyto architektury mohou být správnou volbou pro správce Falcot Corp, zvláště kvůli firewallu (jádro podporuje tři různé firewally: IPF, IPFW, PF) nebo NAS (síťovému datovému úložišti, pro které byl souborový systém ZFS testován a schválen)."

msgid "Why the Debian Distribution?"
msgstr "Proč distribuce Debian?"

msgid "Once the Linux family has been selected, a more specific option must be chosen. Again, there are plenty of criteria to consider. The chosen distribution must be able to operate for several years, since the migration from one to another would entail additional costs (although less than if the migration were between two totally different operating systems, such as Windows or OS X)."
msgstr "Jakmile byla vybrána rodina Linuxu, další konkrétnější volba musí provedena. Znovu, existuje spousta hledisek ke zvážení. Zvolená distribuce musí být schopna provozu po řadu let, protože přechod z jedné na druhou by sebou přinesl další náklady (i když méně, než kdyby se přecházelo mezi úplně odlišnými operačními systémy, jako je Windows nebo OS X)."

msgid "Sustainability is, thus, essential, and it must guarantee regular updates and security patches over several years. The timing of updates is also significant, since, with so many machines to manage, Falcot Corp can not handle this complex operation too frequently. The IT department, therefore, insists on running the latest stable version of the distribution, benefiting from the best technical assistance, and guaranteed security patches. In effect, security updates are generally only guaranteed for a limited duration on older versions of a distribution."
msgstr "Udržitelnost je tedy nezbytná a distribuce musí garantovat pravidelné aktualizace a bezpečnostní opravy po řadu let. Načasování aktualizací je také důležité, protože s tolika zařízeními na starosti, si Falcot Corp nemůže takovou složitou operaci dovolit příliš často. IT oddělení proto trvá na provozování poslední stabilní verze distribuce a chce tak těžit z nejlepší technické podpory a zaručených bezpečnostních oprav. V podstatě, bezpečnostní aktualizace jsou obyčejně zaručeny pouze na omezenou dobu na starších verzích distribuce."

#, fuzzy
#| msgid "Finally, for reasons of homogeneity and ease of administration, the same distribution must run on all the servers (some of which are Sparc machines, currently running Solaris) and office computers."
msgid "Finally, for reasons of homogeneity and ease of administration, the same distribution must run on all the servers and office computers."
msgstr "Nakonec, kvůli jednotnosti a snadné správě, stejná distribuce musí běžet na všech serverech (některé z nich jsou stroje Sparc, na kterých běží Solaris) a kancelářských počítačích."

msgid "Commercial and Community Driven Distributions"
msgstr "Komerční a komunitou řízené distribuce"

msgid "There are two main categories of Linux distributions: commercial and community driven. The former, developed by companies, are sold with commercial support services. The latter are developed according to the same open development model as the free software of which they are comprised."
msgstr "Existují dvě hlavní kategorie distribucí Linuxu: komerční a ty, řízené komunitou. Ty první, vyvinuté podniky, jsou prodávány s komerčními podpůrnými službami. Ty druhé jsou vyvinuté podle stejného vývojářského modelu jako volný software, ze kterého vzešly."

#, fuzzy
#| msgid "A commercial distribution will have, thus, a tendency to release new versions more frequently, in order to better market updates and associated services. Their future is directly connected to the commercial success of their company, and many have already disappeared (Caldera Linux, StormLinux, etc.)."
msgid "A commercial distribution will have, thus, a tendency to release new versions more frequently, in order to better market updates and associated services. Their future is directly connected to the commercial success of their company, and many have already disappeared (Caldera Linux, StormLinux, Mandriva Linux, etc.)."
msgstr "Komerční distribuce budou mít tedy tendenci vydávat nové verze častěji, za účelem lépe prodávat aktualizace a přidružené služby. Jejich budoucnost je přímo spojena s obchodním úspěchem jejich podniku a mnohé z nich už zanikly (Caldera Linux, StormLinux, a další)."

msgid "A community distribution doesn't follow any schedule but its own. Like the Linux kernel, new versions are released when they are stable, never before. Its survival is guaranteed, as long as it has enough individual developers or third party companies to support it."
msgstr "Distribuce komunity nefunguje podle žádného rozpisu, ale funguje podle sebe. Stejně jako jádro Linuxu, tak i nové verze jsou vydávány pokud jsou stabilní, a nikdy dříve. Její přežití je zaručeno, pokud bude mít dostatek vývojářů nebo podniků třetích stran ke své podpoře."

msgid "<primary>distribution</primary><secondary>community Linux distribution</secondary>"
msgstr "<primary>distribuce</primary><secondary>Linuxová distribuce komunity</secondary>"

msgid "<primary>distribution</primary><secondary>commercial Linux distribution</secondary>"
msgstr "<primary>distribuce</primary><secondary>komerční Linuxová distribuce</secondary>"

msgid "A comparison of various Linux distributions led to the choice of Debian for various reasons:"
msgstr "Srovnání různých Linuxových distribucí vedlo k volvě Debianu z různých důvodů:"

msgid "It is a community distribution, with development ensured independently from any commercial constraints; its objectives are, thus, essentially of a technical nature, which seem to favor the overall quality of the product."
msgstr "Je to distribuce komunity, s vývojem zaručeně nezávislým na komerčních omezeních; jejich motivy jsou tedy zásadně technické povahy, což se zdá je ku prospěchu všeobecné kvality produktu."

msgid "Of all community distributions, it is the most significant from many perspectives: in number of contributors, number of software packages available, and years of continuous existence. The size of its community is an incontestable witness to its continuity."
msgstr "Pro všechny distribuce komunit je to to nejpodstatnější z mnoha pohledů: v počtu přispěvatelů, počtu dostupných softwarových balíčků a roků existence bez přerušení. Velikost její komunity neoddiskutovatelně svědčí o její kontinuitě."

msgid "Statistically, new versions are released every 18 to 24 months, and they are supported for 5 years, a schedule which is agreeable to administrators."
msgstr "Statisticky, nové verze jsou vydávány co 18 až 24 měsíců, a jsou podporovány po pět let, plán, který je pro správce odsouhlasitelný."

msgid "A survey of several French service companies specialized in free software has shown that all of them provide technical assistance for Debian; it is also, for many of them, their chosen distribution, internally. This diversity of potential providers is a major asset for Falcot Corp's independence."
msgstr "Výzkum několika francouzských servisních firem, které se specializují na volný software ukázal, že všechny poskytují technickou podoru pro Debian; je také, pro mnohé z nich, jejich interně vybraná distribuce. Tato rozmanitost potenciálních poskytovatelů je hlavní předností co do nezávislosti pro firmu Falcot Corp."

msgid "Finally, Debian is available on a multitude of architectures, including ppc64el for OpenPOWER processors; it will, thus, be possible to install it on Falcot Corp's latest IBM servers."
msgstr "A konečně, Debian je k dispozici na celé spoustě architektur, počínaje ppc64el pro OpenPOWER procesory, až po poslední servery IBM, které Falcot Corp vlastní."

msgid "<emphasis>IN PRACTICE</emphasis> Debian Long Term Support"
msgstr "<emphasis>V PRAXI</emphasis> dlouhodobá podpora Debianu"

msgid "The Debian Long Term Support (LTS) project started in 2014 and aims to provide 5 years of security support to all stable Debian releases. As LTS is of primary importance to organizations with large deployments, the project tries to pool resources from Debian-using companies. <ulink type=\"block\" url=\"https://wiki.debian.org/LTS\" />"
msgstr "Projekt dlouhodobé podpory Debianu (LTS) začal v roce 2014 a má za cíl poskytovat pětiletou bezpečnostní podporu všem stabilním vydáním Debianu. Za prvořadé důležitosti LTS pro organizace s obsažnými nasazeními aplikací, projekt se snaží sdílet zdroje s firmami používajícími Debian. <ulink type=\"block\" url=\"https://wiki.debian.org/LTS\" />"

#, fuzzy
#| msgid "Falcot Corp is not big enough to let one member of its IT staff contribute to the LTS project, so the company opted to subscribe to Freexian's Debian LTS contract and provides financial support. Thanks to this, the Falcot administrators know that the packages they use will be handled in priority and they have a direct contact with the LTS team in case of problems. <ulink type=\"block\" url=\"https://wiki.debian.org/LTS/Funding\" /> <ulink type=\"block\" url=\"http://www.freexian.com/services/debian-lts.html\" />"
msgid "Falcot Corp is not big enough to let one member of its IT staff contribute to the LTS project, so the company opted to subscribe to Freexian's Debian LTS contract and provides financial support. Thanks to this, the Falcot administrators know that the packages they use will be handled in priority and they have a direct contact with the LTS team in case of problems. <ulink type=\"block\" url=\"https://wiki.debian.org/LTS/Funding\" /> <ulink type=\"block\" url=\"https://www.freexian.com/services/debian-lts.html\" />"
msgstr "Podnik Falcot Corp není dostatečně velký na to, aby mohl jednoho člena IT týmu nechat spolupracovat na projektu LTS, tak se firma zavázala upsat na smlouvu LTS Debianu firmy Freexian a poskytnout finanční podporu. Díky tomu správci Falcotu vědí, že balíčky, které používají, budou obhospodařovány přednostně a že budou mít přímý kontakt s týmem LTS v případě problémů."

#, fuzzy
#| msgid "Once Debian has been chosen, the matter of which version to use must be decided. Let us see why the administrators have picked Debian Jessie."
msgid "Once Debian has been chosen, the matter of which version to use must be decided. Let us see why the administrators have picked Debian <emphasis role=\"distribution\">Buster</emphasis>."
msgstr "Když byl vybrán Debian, otázka, kterou verzi používat musí být zodpovězena. Podívejme se, proč správci vybrali Debian Jessie."

#, fuzzy
#| msgid "Why Debian Jessie?"
msgid "Why Debian Buster?"
msgstr "Proč Debian Jessie?"

#, fuzzy
#| msgid "Every Debian release starts its life as a continuously changing distribution, also known as “<emphasis role=\"distribution\">Testing</emphasis>”. But at the time we write those lines, Debian Jessie is the latest “<emphasis role=\"distribution\">Stable</emphasis>” version of Debian."
msgid "Every Debian release starts its life as a continuously changing distribution, also known as “<emphasis role=\"distribution\">Testing</emphasis>”. But at the time you read those lines, Debian <emphasis role=\"distribution\">Buster</emphasis> is the latest “<emphasis role=\"distribution\">Stable</emphasis>” version of Debian."
msgstr "Každé vydání Debianu započalo svoji existenci jako nepřetržitě se měnící distribuce, také známá jako “<emphasis role=\"distribution\">Testovací</emphasis>”. Ale v době, kdy píšeme tyto řádky, je Debian Jessie poslední “<emphasis role=\"distribution\">stabilní</emphasis>” verzí Debianu."

#, fuzzy
#| msgid "The choice of Debian Jessie is well justified based on the fact that any administrator concerned about the quality of their servers will naturally gravitate towards the stable version of Debian. Even if the previous stable release might still be supported for a while, Falcot administrators aren't considering it because its support period will not last long enough and because the latest version brings new interesting features that they care about."
msgid "The choice of Debian <emphasis role=\"distribution\">Buster</emphasis> is well justified based on the fact that any administrator concerned about the quality of their servers will naturally gravitate towards the stable version of Debian. Even if the previous stable release might still be supported for a while, Falcot administrators aren't considering it because its support period will not last long enough and because the latest version brings new interesting features that they care about."
msgstr "Volba Debianu Jessie je dobře odůvodněna faktem, že kterýkoliv správce se zájmem o kvalitu svých serverů bude přirozeně tíhnout ke stabilní verzi Debianu. I když předchozí stabilní verze může být ještě po nějakou dobu podporována, správci Falcotu o ní nepřemýšlí, protože její podpora nepotrvá dostatečně dlouho a protože poslední verze přináší nové zajímavé prvky, o které mají zájem."

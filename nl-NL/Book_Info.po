# Patrick Kox <patrick.kox@proximus.be>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2020-05-17 17:54+0200\n"
"PO-Revision-Date: 2017-04-29 11:26+0200\n"
"Last-Translator: Patrick Kox <patrick.kox@proximus.be>\n"
"Language-Team: Dutch; Flemish <Debian Administrator Handbook Translators Maillist <debian-handbook-translators@lists.alioth.debian.org>>\n"
"Language: nl-NL\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Gtranslator 2.91.7\n"

msgid "The Debian Administrator's Handbook"
msgstr "Het Debian Beheerders Handboek"

#, fuzzy
#| msgid "Debian Jessie from Discovery to Mastery"
msgid "Debian Buster from Discovery to Mastery"
msgstr "Debian Jessie van Ontdekking tot Meesterschap"

msgid "Debian"
msgstr "Debian"

msgid "A reference book presenting the Debian distribution, from initial installation to configuration of services."
msgstr "Een referentie die de Debian distributie presenteert, van initiële installatie tot de configuratie van diensten."

#, fuzzy
#| msgid "ISBN: 979-10-91414-04-3 (English paperback)"
msgid "ISBN: 979-10-91414-19-7 (English paperback)"
msgstr "ISBN: 979-10-91414-04-3 (Engelstalige paperback)"

#, fuzzy
#| msgid "ISBN: 979-10-91414-05-0 (English ebook)"
msgid "ISBN: 979-10-91414-20-3 (English ebook)"
msgstr "ISBN: 979-10-91414-05-0 (Engelstalig e-boek)"

msgid "This book is available under the terms of two licenses compatible with the Debian Free Software Guidelines."
msgstr "Dit boek is beschikbaar onder de twee licenties compatibel met de Debian Vrije Software Richtlijnen."

msgid "Creative Commons License Notice:"
msgstr "Creative Commons License Nota:"

#, fuzzy
#| msgid "This book is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License. <ulink type=\"block\" url=\"http://creativecommons.org/licenses/by-sa/3.0/\" />"
msgid "This book is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License. <ulink type=\"block\" url=\"https://creativecommons.org/licenses/by-sa/3.0/\" />"
msgstr "Dit boek is vrijgegeven onder de Creative Commons Attribution-ShareAlike 3.0 Unported Licentie. <ulink type=\"block\" url=\"http://creativecommons.org/licenses/by-sa/3.0/\" />"

msgid "GNU General Public License Notice:"
msgstr "GNU Algemene Publieke Licentie Nota:"

msgid "This book is free documentation: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version."
msgstr "Dit boek is vrije documentatie: je kunt het herverdelen en/of aanpassen onder de regels van de GNU Algemene Publieke Licentie zoals gepubliceerd door de Free Software Foundation, zowel versie 2 van de Licentie of (naar uw keuze) enige latere versie."

msgid "This book is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details."
msgstr "Dit boek is verdeeld in de hoop dat het bruikbaar zal zijn, maar ZONDER ENIGE GARANTIE; zelfs zonder de geïmpliceerde garantie van VERKOOPBAARHEID of GESCHIKTHEID VOOR EEN BEPAALD DOEL. Zie de GNU Algemene Publieke Licentie voor meer informatie."

#, fuzzy
#| msgid "You should have received a copy of the GNU General Public License along with this program. If not, see <ulink url=\"http://www.gnu.org/licenses/\" />."
msgid "You should have received a copy of the GNU General Public License along with this program. If not, see <ulink url=\"https://www.gnu.org/licenses/\" />."
msgstr "Je zou een kopie van de GNU Algemene Publieke Licentie bij die programma moeten krijgen. Indien niet het geval, zie <ulink url=\"http://www.gnu.org/licenses/\" />."

msgid "Show your appreciation"
msgstr "Toon uw waardering"

#, fuzzy
#| msgid "This book is published under a free license because we want everybody to benefit from it. That said maintaining it takes time and lots of effort, and we appreciate being thanked for this. If you find this book valuable, please consider contributing to its continued maintenance either by buying a paperback copy or by making a donation through the book's official website: <ulink type=\"block\" url=\"http://debian-handbook.info\" />"
msgid "This book is published under a free license because we want everybody to benefit from it. That said maintaining it takes time and lots of effort, and we appreciate being thanked for this. If you find this book valuable, please consider contributing to its continued maintenance either by buying a paperback copy or by making a donation through the book's official website: <ulink type=\"block\" url=\"https://debian-handbook.info\" />"
msgstr "Dit boek is uitgegeven onder een vrije licentie want we willen dat iedereen ervan kan genieten. Dat gezegd zijnde houdt het onderhouden veel tijd en werk in, en we waarderen het om hiervoor bedankt te worden. Als u dit boet bruikbaar vindt, denk er dan alstublieft aan om bij te dragen aan zijn doorlopende onderhoud hetzij door een papieren kopie te kopen of door een schenking te doen via de officiële website van het boek: <ulink type=\"block\" url=\"http://debian-handbook.info\" />"
